FROM python:3.8.0-slim-buster

WORKDIR /app
COPY . ./

RUN pip install -r requirements.txt

EXPOSE 80

ENTRYPOINT ["python"]
CMD ["app.py"]
