from flask import Flask

import json
import mysql.connector
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    config = {
        'user': os.environ['MYSQL_ROOT_USER'],
        'password': os.environ['MYSQL_ROOT_PASSWORD'],
        'host': os.environ['MYSQL_HOST'],
        'port': os.environ['MYSQL_PORT'],
        'database': 'knights'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM favorite_colors')
    results = [{name: color} for (name, color) in cursor]
    cursor.close()
    connection.close()

    return json.dumps({ 'favourite_colours': results })

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
